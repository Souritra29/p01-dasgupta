//
//  ViewController.h
//  hello world
//
//  Created by Souritra Dasgupta on 1/23/17.
//  Copyright © 2017 Souritra Dasgupta. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController

@property (nonatomic, strong) IBOutlet UILabel *hlabel;
-(IBAction)onclick:(id)sender;
@property (nonatomic, assign) NSInteger count;

@end

