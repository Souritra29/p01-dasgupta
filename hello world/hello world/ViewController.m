//
//  ViewController.m
//  hello world
//
//  Created by Souritra Dasgupta on 1/23/17.
//  Copyright © 2017 Souritra Dasgupta. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController
@synthesize hlabel;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

int count = 0;

-(IBAction)onclick:(id)sender
{
    if (count == 0) {
    [hlabel setText:@"What do you want kid?"];
        count = count + 1;
    }
    else if (count == 1)
    {
        [hlabel setText:@"Stop bothering me! Go away."];
        count = count + 1;
    }
    else if (count == 2)
    {
        [hlabel setText:@"Blistering barnacles! Just leave me alone!"];
        count = count + 1;
    }
    else if (count == 3)
    {
        [hlabel setText:@"Don't you dare come near me, you lily-livered landlubber!"];
        count = count + 1;
    }
    else if (count == 4)
    {
        [hlabel setText:@"I'll turn you into a hearth-rug you hear me?"];
        count = 0;
    }
}
@end


