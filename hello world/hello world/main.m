//
//  main.m
//  hello world
//
//  Created by Souritra Dasgupta on 1/23/17.
//  Copyright © 2017 Souritra Dasgupta. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
